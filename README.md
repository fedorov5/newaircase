# Prerequisites

> #### Node.js (^10.12.0, or >=12.0.0)

---

#### Get `started`

```console

	cd webpack-boilerplate
	npm i
	npm start
```

#### For `build`

```console
	npm run dev 	// development
	npm run build	// production
```

#### For `lint`

> - [stylelint][stylelint]
> - [eslint][eslint]

```console
	npm run lint		// lint Javascript files
	npm run stylelint	// lint SCSS files
```

---

# Additionally for the VS Code

### Extensions

> - #### ESLint

> - #### Stylelint

`settings.json`

```json
	"stylelint.ignoreDisables": true,
```

---
