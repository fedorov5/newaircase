import { gsap, ScrollTrigger, TweenLite, TimelineMax } from "gsap/all";
gsap.registerPlugin(ScrollTrigger);


gsap.to("#bokeh3", {
  scrollTrigger: {
    trigger: ".container_store",
    start: "-150px center",
    end: "bottom center",
    toggleActions: "play pause reverse none",
    onEnter: () => {
      let vid3 = document.getElementById("bokeh3");
      vid3.play();
    },
  },
  duration: 3,
});

gsap.to("#bokeh4", {
  scrollTrigger: {
    trigger: ".container_store",
    start: "-150px center",
    end: "bottom center",
    toggleActions: "play pause reverse none",
    onEnter: () => {
      let vid4 = document.getElementById("bokeh4");
      vid4.play();
    },
  },
  duration: 3,
});

// gsap.to(".zoom", {
//   scrollTrigger: {
//     trigger: ".recipes_left",
//     start: "top center",
//     // markers: true,
//     scrub: true, // scrub: 3  остановка при 3с плавная
//     toggleActions: "restart pause reverse pause",
//   },
//   scale: 1.2,
//   duration: 3,
// });

// gsap.to("#bias1, #bias2", {
//   scrollTrigger: {
//     trigger: ".recipes_right",
//     start: "top center",
//     // markers: true,
//     scrub: true, // scrub: 3  остановка при 3с плавная
//     toggleActions: "restart pause reverse pause",
//   },
//   y: 0,
//   duration: 3,
// });

let team = document.querySelector(".bottom_team");
let canvas = document.getElementById("canvas");
let windowHeight = window.innerHeight;

window.addEventListener("scroll", function (e) {
  let heightOnView = team.getBoundingClientRect().top;
  if (heightOnView < windowHeight && heightOnView > 0) {
    canvas.style.transform = `translateY(${heightOnView - canvas.offsetHeight}px)`;
  }
  if (windowHeight - heightOnView < 40) {
    canvas.style.transform = `translateY(0px)`;
  }
});

let scrollTrig1 = document.querySelector(".scrollTrig1");
let showCase = document.querySelector(".show_case");
// let footer = document.querySelector(".footer");

window.addEventListener("scroll", function (e) {
  let heightOnView = Math.round(scrollTrig1.getBoundingClientRect().top) ;
  // console.log('tag', heightOnView)
  if (heightOnView < windowHeight && heightOnView > 0) {
    showCase.style.clipPath = `polygon(0 0, 100% 0, 100% ${heightOnView}px, 0 ${heightOnView + 150*Math.pow(heightOnView/windowHeight,2)}px)`; // clip-path: polygon(0 0, 100% 0, 100% 74%, 0 65%);
  }
  if (windowHeight - heightOnView < 40) {
    showCase.style.clipPath = `polygon(0 0, 100% 0, 100% 100%, 0 100%)`;
  }
  if (heightOnView < 400) {
    // footer.style.zIndex = "1";
    document.querySelector("body").style.pointerEvents = "none";
    document.querySelector(".footer").style.pointerEvents = "auto";
  }
  if (heightOnView > 400) {
    // footer.style.zIndex = "-1";
    document.querySelector("body").style.pointerEvents = "unset";
    document.querySelector(".footer").style.pointerEvents = "unset";
  }
});
