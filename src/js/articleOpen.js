import { TimelineMax } from "gsap/all";

let tl = new TimelineMax();

let title1 = document.querySelector(".title1");
let title2 = document.querySelector(".title2");
let article1 = document.querySelector(".article1");
let article2 = document.querySelector(".article2");
let article1IsOpen = false;
let article2IsOpen = false;
let windowWight = window.innerWidth;

title1.addEventListener("click", openArticle1);

window.addEventListener("resize", funcResize);

function funcResize() {
  windowWight = window.innerWidth;
  if (windowWight > 650) {
    article1.style.height = "auto";
    article2.style.height = "auto";
  } else {
    article1.style.height = "0";
    article2.style.height = "0";
    article1IsOpen = false;
    article2IsOpen = false;
  }
}

function openArticle1() {
  if (windowWight < 650) {
    if (article1IsOpen) {
      tl.to(article1, 1, {
        height: "0",
        ease: "power2.out",
      });
      article1IsOpen = false;
    } else {
      tl.to(article1, 1, {
        height: "auto",
        ease: "power2.out",
      });
      article1IsOpen = true;
    }
  }
}

title2.addEventListener("click", openArticle2);

function openArticle2() {
  if (windowWight < 650) {
    if (article2IsOpen) {
      tl.to(article2, 1, {
        height: "0",
        ease: "power2.out",
      });
      article2IsOpen = false;
    } else {
      tl.to(article2, 1, {
        height: "auto",
        ease: "power2.out",
      });
      article2IsOpen = true;
    }
  }
}
