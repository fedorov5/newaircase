import { gsap, ScrollTrigger } from "gsap/all";

gsap.registerPlugin(ScrollTrigger);

gsap.to(".tasks_hr1", {
  scrollTrigger: {
    trigger: ".tasks_hr1",
    toggleActions: "restart pause resume reverse",
  },
  width: "100%",
  duration: 2,
});

gsap.to(".tasks_hr2", {
  scrollTrigger: {
    trigger: ".tasks_hr2",
    toggleActions: "restart pause resume reverse",
  },
  width: "100%",
  duration: 2,
});



gsap.to(".tasks_hr4", {
  scrollTrigger: {
    trigger: ".tasks_hr4",
    toggleActions: "restart pause resume reverse",
  },
  width: "100%",
  duration: 2,
});

gsap.to(".text_hr", {
  scrollTrigger: {
    trigger: ".text_hr",
    toggleActions: "restart pause resume reverse",
  },
  width: "100%",
  duration: 2,
});
