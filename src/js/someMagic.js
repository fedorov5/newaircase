import { gsap, ScrollTrigger } from "gsap/all";
// gsap.registerPlugin(ScrollTrigger);


gsap.to(".img1, .img2, .img3", {
  scrollTrigger: {
    trigger: ".hero_img_block",
    start: "100px center",
    end: "500px center",
    // markers: true,
    scrub: true, // scrub: 3  остановка при 3с плавная
    toggleActions: "restart pause reverse pause",
    onEnter: () => {},
  },
  duration: 3,
  rotation: 0,
  scale: 1.3,
});

let angle = 0;

gsap.to(".prod1,.prod2,.prod3,.prod4,.prod5,.prod6,.prod7,.prod8", {
  scrollTrigger: {
    trigger: ".product_section",
    start: "0px center",
    end: "40% center",
    // markers: true,
    scrub: true, // scrub: 3  остановка при 3с плавная
    toggleActions: "restart pause reverse pause",
  },
  duration: 3,
  rotation: 0,
  scale: 1.6,
});

gsap.to(".prod1,.prod2,.prod3,.prod4,.prod5,.prod6,.prod7,.prod8", {
  scrollTrigger: {
    trigger: ".product_section",
    start: "60% center",
    end: "bottom center",
    // markers: true,
    immediateRender: false,
    scrub: true, // scrub: 3  остановка при 3с плавная
    toggleActions: "restart pause reverse pause",

  },
  duration: 3,
  rotation: -45,
  scale: 1,
});

// ScrollTrigger.create({
//   trigger: ".store_wrapper",
//   start: "top top+=75",
//   // markers: true,
//   endTrigger: ".stiky",
//   end: "bottom bottom",
//   pin: true,
//   pinSpacing: false,
// });

// let windowWight = window.innerWidth;

// if (windowWight > 650) {
//   ScrollTrigger.create({
//     trigger: ".product_container",
//     start: "top top+=75",
//     end: "bottom bottom",
//     // markers: true,
//     endTrigger: ".stiky_prod",
//     pin: true,
//     pinSpacing: false,
//   });
// }




